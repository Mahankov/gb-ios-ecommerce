//
//  ChangeUserData.swift
//  GBShop
//
//  Created by Anton Makhankov on 16.04.2020.
//  Copyright © 2020 Anton Makhankov. All rights reserved.
//

import Alamofire

extension Auth {

    func changeUserData(id_user: Int, username: String, password: String, email: String, gender: String, credit_card: String, bio: String, completionHandler: @escaping (DataResponse<CommonResult>) -> Void) {
        let requestModel = ChangeUserData(baseUrl: baseUrl, id_user: id_user, username: username, password: password, email: email, gender: gender, credit_card: credit_card, bio: bio)
        self.request(request: requestModel, completionHandler: completionHandler)
    }
    
}

extension Auth {
    struct ChangeUserData: RequestRouter {
        let baseUrl: URL
        let method: HTTPMethod = .get
        let path: String = "changeUserData.json"
        
        let id_user: Int
        let username: String
        let password: String
        let email: String
        let gender: String
        let credit_card: String
        let bio: String

        var parameters: Parameters? {
            return [
                "user_id": id_user,
                "username": username,
                "password": password,
                "email": email,
                "gender": gender,
                "credit_card": credit_card,
                "bio": bio

            ]
        }
    }
}

