//
//  RegisterUser.swift
//  GBShop
//
//  Created by Anton Makhankov on 16.04.2020.
//  Copyright © 2020 Anton Makhankov. All rights reserved.
//

import Alamofire

extension Auth {
    
    func registerUser(username: String, password: String, email: String, gender: String, credit_card: String, bio: String, completionHandler: @escaping (DataResponse<RegisterResult>) -> Void) {
        let requestModel = Register(baseUrl: baseUrl, username: username, password: password, email: email, gender: gender, credit_card: credit_card, bio: bio)
        self.request(request: requestModel, completionHandler: completionHandler)
    }
    
}

extension Auth {
    struct Register: RequestRouter {
        let baseUrl: URL
        let method: HTTPMethod = .get
        let path: String = "registerUser.json"
        
        let username: String
        let password: String
        let email: String
        let gender: String
        let credit_card: String
        let bio: String

        var parameters: Parameters? {
            return [
                "username": username,
                "password": password,
                "email": email,
                "gender": gender,
                "credit_card": credit_card,
                "bio": bio

            ]
        }
    }
}
