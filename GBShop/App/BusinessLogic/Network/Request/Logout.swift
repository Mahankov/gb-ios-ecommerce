//
//  Logout.swift
//  GBShop
//
//  Created by Anton Makhankov on 16.04.2020.
//  Copyright © 2020 Anton Makhankov. All rights reserved.
//

import Alamofire

extension Auth {
    
    func logout(user_id: Int, completionHandler: @escaping (DataResponse<CommonResult>) -> Void) {
        let requestModel = Logout(baseUrl: baseUrl, user_id: user_id)
        self.request(request: requestModel, completionHandler: completionHandler)
    }
    
}

extension Auth {
    struct Logout: RequestRouter {
        let baseUrl: URL
        let method: HTTPMethod = .get
        let path: String = "logout.json"
        
        let user_id: Int
        var parameters: Parameters? {
            return [
                "user_id": user_id
            ]
        }
    }
}
