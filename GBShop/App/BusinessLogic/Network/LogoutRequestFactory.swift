//
//  LogoutRequestFactory.swift
//  GBShop
//
//  Created by Anton Makhankov on 14.04.2020.
//  Copyright © 2020 Anton Makhankov. All rights reserved.
//

import Alamofire

protocol LogoutRequestFactory {
    func logout(user_id: Int, completionHandler: @escaping (DataResponse<LogoutResult>) -> Void)
}
