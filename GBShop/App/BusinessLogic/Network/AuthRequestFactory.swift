//
//  AuthRequestFactory.swift
//  GBShop
//
//  Created by Anton Makhankov on 14.04.2020.
//  Copyright © 2020 Anton Makhankov. All rights reserved.
//

import Alamofire

protocol AuthRequestFactory {
    
    func login(
        userName: String,
        password: String,
        completionHandler: @escaping (DataResponse<LoginResult>) -> Void)
    
    func registerUser(
        username: String,
        password: String,
        email: String,
        gender: String,
        credit_card: String,
        bio: String,
        completionHandler: @escaping (DataResponse<RegisterResult>) -> Void)
    
    func changeUserData(
        id_user: Int,
        username: String,
        password: String,
        email: String,
        gender: String,
        credit_card: String,
        bio: String,
        completionHandler: @escaping (DataResponse<CommonResult>) -> Void)
    
    func logout(
        user_id: Int,
        completionHandler: @escaping (DataResponse<CommonResult>) -> Void)
    
}
