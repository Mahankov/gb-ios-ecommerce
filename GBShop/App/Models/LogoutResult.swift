//
//  LogoutResult.swift
//  GBShop
//
//  Created by Anton Makhankov on 14.04.2020.
//  Copyright © 2020 Anton Makhankov. All rights reserved.
//

struct LogoutResult: Codable {
    let result: Int
}
