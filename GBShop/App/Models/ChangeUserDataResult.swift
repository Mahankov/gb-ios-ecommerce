//
//  ChangeUserDataResult.swift
//  GBShop
//
//  Created by Anton Makhankov on 15.04.2020.
//  Copyright © 2020 Anton Makhankov. All rights reserved.
//

struct ChangeUserDataResult: Codable {
    let result: Int
}
