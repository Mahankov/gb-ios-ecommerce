//
//  User.swift
//  GBShop
//
//  Created by Anton Makhankov on 14.04.2020.
//  Copyright © 2020 Anton Makhankov. All rights reserved.
//

struct User: Codable {
    let id: Int
    let login: String
    let name: String
    let lastname: String
    
    enum CodingKeys: String, CodingKey {
        case id = "id_user"
        case login = "user_login"
        case name = "user_name"
        case lastname = "user_lastname"
    }
}
