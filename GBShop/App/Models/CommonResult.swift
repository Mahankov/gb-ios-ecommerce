//
//  CommonResult.swift
//  GBShop
//
//  Created by Anton Makhankov on 16.04.2020.
//  Copyright © 2020 Anton Makhankov. All rights reserved.
//

struct CommonResult: Codable {
    let result: Int
}
