//
//  AbstractErrorParser.swift
//  GBShop
//
//  Created by Anton Makhankov on 14.04.2020.
//  Copyright © 2020 Anton Makhankov. All rights reserved.
//

import Foundation

protocol AbstractErrorParser {
    func parse(_ result: Error) -> Error
    func parse(response: HTTPURLResponse?, data: Data?, error: Error?) -> Error?
}
